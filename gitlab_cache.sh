#!/bin/sh

quiet_docker() {
  grep -e 'Pulling from' -e 'Digest' -e 'Status' -e 'Error' -e 'The push refers to repository' -e 'digest:'
}

pull_gitlab_cache() {
  docker pull $1 | quiet_docker || docker pull $2 | quiet_docker || true
  docker tag $1 $3 || docker tag $2 $3 || true
}

push_gitlab_cache() {
  docker tag $2 $1 || true
  docker push $1 | quiet_docker || true
}
