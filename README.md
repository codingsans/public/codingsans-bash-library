# Coding Sans Bash Library

## Gitlab Cache

Import:
```shell
eval "$(curl -s https://gitlab.com/codingsans/public/coding-sans-bash-library/raw/master/gitlab_cache.sh)"
```
OR
```shell
eval "$(curl -s https://gitlab.com/codingsans/public/coding-sans-bash-library/raw/[COMMIT OR BRANCH ID]/gitlab_cache.sh)"
```

2 commands included:

### Pull Gitlab Cache

Tries to pull the current cache, or the fallback cache, and tag them to the local image name.

```shell
pull_gitlab_cache [CURRENT_CACHE_NAME] [FALLBACK_CACHE_NAME] [LOCAL_IMAGE_NAME]
```

- CURRENT_CACHE_NAME: This is the current build specific docker image name for the cache
- FALLBACK_CACHE_NAME: This is the fallback docker image name eg.: master's
- LOCAL_IMAGE_NAME: Local alias for the image, like `my-super-project_app`

### Push Gitlab Cache

Tries to push the local cache to the current cache.

```shell
push_gitlab_cache [CURRENT_CACHE_NAME] [LOCAL_IMAGE_NAME]
```

- CURRENT_CACHE_NAME: This is the current build specific docker image name for the cache
- LOCAL_IMAGE_NAME: Local alias for the image, like `my-super-project_app`